# WeatherFlux

## Description

ReactiveWeather is a reactive microservice designed to fetch and provide weather data. Developed using Kotlin and the Spring WebFlux framework, the service is designed to be scalable and efficient, ensuring real-time weather information retrieval.

## Key Features

1. Reactive Data Fetching: Utilizing the reactive capabilities of Spring WebFlux for non-blocking data access.
2. Temperature Conversion: Offers on-the-fly conversion of temperature values from Fahrenheit to Celsius.
3. Error Handling: Provides robust error handling, including specific defect types for various scenarios, ensuring transparent communication to the frontend.
4. Endpoint for Today's Weather: Offers an endpoint (`/today-weather`) that delivers the day's weather in a structured format.

## Technical Stack

- **Language**: Kotlin
- **Framework**: Spring Boot with WebFlux
- **Error Handling**: WebClientResponseException handling for specific error scenarios.

## Getting Started

1. Clone the repository: `git clone git@gitlab.com:lubere/weatherflux.git`
2. Navigate to the project directory: `cd ReactiveWeather`
3. Build and run the project

## Endpoints

- `/today-weather`: Fetches and provides weather details for the current day.