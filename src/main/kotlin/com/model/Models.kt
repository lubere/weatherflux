package com.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class WeatherResponse @JsonCreator constructor(
    @JsonProperty("properties") val properties: WeatherProperties?
)

data class WeatherProperties @JsonCreator constructor(
    @JsonProperty("periods") val periods: List<WeatherPeriod>?
)

data class WeatherPeriod @JsonCreator constructor(
    @JsonProperty("name") val name: String,
    @JsonProperty("temperature") val temperature: Double,
    @JsonProperty("shortForecast") val shortForecast: String,
)

data class DailyForecast(
    val dayName: String,
    val tempHighCelsius: Double,
    val forecastBlurp: String,
)

/**
 * @property forecast Contains the daily weather forecast details.
 * @property defect Provides information about any errors or issues that occurred while fetching or processing the weather data.
 */
data class ResponseDetails(val forecast: DailyForecast?, val defect: DefectType?)

enum class DefectType {
    SERVER_ERROR, // for 5xx errors
    CLIENT_ERROR, // for 4xx errors
    OTHER_ERROR,   // for all other exceptions
    NO_DATA, // empty result
}
