package com

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class JacksonConfig {

    /**
     * Provides a customized ObjectMapper for Jackson
     *
     * This ObjectMapper is configured to use snake_case as its default naming strategy
     * for serialization and deserialization
     */
    @Bean
    open fun objectMapper(): ObjectMapper {
        return ObjectMapper().apply {
            propertyNamingStrategy = PropertyNamingStrategies.SNAKE_CASE
        }
    }
}