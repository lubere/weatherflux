package com.service

import com.model.*
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientResponseException
import reactor.core.publisher.Mono

@Service
class WeatherService(val webClient: WebClient = WebClient.create("https://api.weather.gov")) {

    /**
     * Fetches the weather for the current day
     *
     * @return Mono<ResponseDetails> A Mono containing the response details which include the weather or error details
     */
    fun fetchTodayWeather(): Mono<ResponseDetails> {
        return webClient.get()
            .uri("/gridpoints/MLB/33,70/forecast")
            .retrieve()
            .bodyToMono(WeatherResponse::class.java)
            .flatMap {
                val periods = it.properties?.periods
                    ?: return@flatMap Mono.just(ResponseDetails(null, DefectType.OTHER_ERROR))
                Mono.just(convertToResponseDetails(periods.getOrNull(0)!!))
            }
            .switchIfEmpty(Mono.just(ResponseDetails(null, DefectType.NO_DATA)))
            .onErrorResume(this::handleError)
    }

    /**
     * Converts a WeatherPeriod object into a ResponseDetails object
     *
     * This function takes the WeatherPeriod data, converts temperature from Fahrenheit to Celsius,
     * and then wraps it in a ResponseDetails object
     */
    fun convertToResponseDetails(response: WeatherPeriod): ResponseDetails {
        val celsiusTemperature = (response.temperature - 32) * 5.0 / 9.0
        return ResponseDetails(DailyForecast(response.name, celsiusTemperature, response.shortForecast), null)
    }

    /**
     * Handles errors that might occur during the fetch operation
     *
     * @param throwable The error or exception thrown during data fetching
     * @return Mono<ResponseDetails> A Mono containing ResponseDetails with the error details
     */
    private fun handleError(throwable: Throwable): Mono<ResponseDetails> {
        // Log the actual error for server-side diagnostic purposes
        println("Error: ${throwable.printStackTrace()}")

        val defectType = when {
            throwable is WebClientResponseException && throwable.statusCode.is5xxServerError -> DefectType.SERVER_ERROR
            throwable is WebClientResponseException && throwable.statusCode.is4xxClientError -> DefectType.CLIENT_ERROR
            else -> DefectType.OTHER_ERROR
        }
        return Mono.just(ResponseDetails(null, defectType))
    }
}
