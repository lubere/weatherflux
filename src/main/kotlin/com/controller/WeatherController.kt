package com.controller

import com.model.ResponseDetails
import com.service.WeatherService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class WeatherController(private val weatherService: WeatherService) {

    /**
     * Endpoint to retrieve weather details for the current day
     *
     * @return Mono<ResponseDetails> A Mono containing the response details which include the weather or error details
     */
    @GetMapping("/today-weather")
    fun getTodayWeather(): Mono<ResponseDetails> {
        return weatherService.fetchTodayWeather()
    }
}