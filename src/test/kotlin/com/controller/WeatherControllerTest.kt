package com.controller

import com.model.DailyForecast
import com.model.DefectType
import com.model.ResponseDetails
import com.service.WeatherService
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import reactor.core.publisher.Mono
import reactor.test.StepVerifier

@SpringBootTest
class WeatherControllerTests(@Autowired val controller: WeatherController) {

    @MockBean
    lateinit var weatherService: WeatherService

    companion object {
        @JvmStatic
        fun defectTypes() = DefectType.entries
    }

    @ParameterizedTest
    @MethodSource("defectTypes")
    fun `test various defects`(defectType: DefectType) {
        given(weatherService.fetchTodayWeather())
            .willReturn(Mono.just(ResponseDetails(null, defectType)))

        val result = controller.getTodayWeather()

        StepVerifier.create(result)
            .expectNext(ResponseDetails(null, defectType))
            .verifyComplete()
    }

    @Test
    fun `test successful fetch of weather data`() {
        val forecast = DailyForecast("Monday", 25.0, "Sunny")
        given(weatherService.fetchTodayWeather())
            .willReturn(Mono.just(ResponseDetails(forecast, null)))

        val result = controller.getTodayWeather()

        StepVerifier.create(result)
            .expectNext(ResponseDetails(forecast, null))
            .verifyComplete()
    }
}
