package com.service

import com.model.WeatherPeriod
import com.model.WeatherProperties
import com.model.WeatherResponse
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.*
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import kotlin.test.assertEquals

@ExtendWith(MockitoExtension::class)
class WeatherServiceTest {

    lateinit var weatherService: WeatherService

    @Mock
    lateinit var webClient: WebClient

    @BeforeEach
    fun setup() {
        weatherService = WeatherService(webClient)
    }

    @Test
    fun `test successful fetch of weather`() {
        val period = WeatherPeriod("Monday", 77.0, "Sunny")
        val properties = WeatherProperties(listOf(period))
        val response = WeatherResponse(properties)

        val mockResponseSpec: WebClient.ResponseSpec = mock()
        val mockRequestHeadersUriSpec: WebClient.RequestHeadersUriSpec<*> = mock()
        val mockRequestHeadersSpec: WebClient.RequestHeadersSpec<*> = mock()

        given(webClient.get())
            .willReturn(mockRequestHeadersUriSpec)
        given(mockRequestHeadersUriSpec.uri(any<String>()))
            .willReturn(mockRequestHeadersSpec)
        given(mockRequestHeadersSpec.retrieve())
            .willReturn(mockResponseSpec)
        given(mockResponseSpec.bodyToMono(WeatherResponse::class.java))
            .willReturn(Mono.just(response))

        val result = weatherService.fetchTodayWeather().block()

        SoftAssertions.assertSoftly { softly ->
            softly.assertThat(result).isNotNull
            softly.assertThat(result?.forecast?.dayName).isEqualTo("Monday")
            softly.assertThat(result?.forecast?.tempHighCelsius)
                .isEqualTo((77.0 - 32) * 5.0 / 9.0)  // Conversion from F to C
            softly.assertThat(result?.forecast?.forecastBlurp).isEqualTo("Sunny")
            softly.assertThat(result?.defect).isNull()
        }
    }

    @Test
    fun `test temperature conversion from Fahrenheit to Celsius`() {
        val givenFahrenheit = 212.0
        val expectedCelsius = 100.0

        val weatherPeriod = WeatherPeriod("Day", givenFahrenheit, "Sunny")

        val resultResponseDetails = weatherService.convertToResponseDetails(weatherPeriod)

        assertEquals(
            expectedCelsius,
            resultResponseDetails.forecast?.tempHighCelsius,
            "Temperature conversion from Fahrenheit to Celsius failed!"
        )
    }
}